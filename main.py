import random
from copy import deepcopy

liste_personnes = [['Pascal', 'ROSE', 43],
                   ['Mickaël', 'FLEUR', 29],
                   ['Henri', 'TULIPE', 35],
                   ['Michel', 'FRAMBOISE', 35],
                   ['Arthur', 'PETALE', 35],
                   ['Michel', 'POLLEN', 50],
                   ['Michel', 'FRAMBOISE', 42]]
random.shuffle(liste_personnes)


def trieur_liste(liste):
    """"Cette fonction organise la liste 'liste_personnes' par leurs prénoms dans l'ordre alphabétique"""
    n = len(liste) # calcule la longueur de la liste renseignées lors de l'appelle de la fonction
    for i in range(n):
        for j in range(0, n - i - 1):
            if liste[j][0] > liste[j + 1][0]: # compare les prénoms des éléments j et j+1
                liste[j], liste[j + 1] = liste[j + 1], liste[j] # échange les elements j et j+1
    return liste


def tri_bulles_inverse(liste):
    """Cette fonction organiste la liste 'liste_personnes' par leurs ages dans l'ordre croissant"""
    n = len(liste)
    for i in range(n):
        for j in range(0, n - i - 1):
            if liste[j][0] < liste[j + 1][0]:
                liste[j], liste[j + 1] = liste[j + 1], liste[j]
    return liste


def tri_bulles_age(liste):
    """Cette fonction organiste la liste 'liste_personnes' par leurs ages dans l'ordre croissant"""
    n = len(liste)
    for i in range(n):
        for j in range(0, n - i - 1):
            if liste[j][2] > liste[j + 1][2]:
                liste[j], liste[j + 1] = liste[j + 1], liste[j]
    return liste


def choix_fonction():
    """Cette fonction offre le choix pour faire appelle à une des 3 fonctions précédentes,
     permet de trier par Prenoms dans l'ordre croissant/décroissant, ou l'âge par ordre croissant"""
    x = 0
    print(
        "Veuillez choisir quelle liste vous voulez afficher:\n"
        "'1'=Prenoms croissants, '2'=Prenoms décroissants, '3'=Age croissant.\n"
        "Vous pouvez arrêter à tout moment en écrivant simplement 'stop'")
    while x < 1:
        choix = input()
        if choix == '1':
            print(trieur_liste(deepcopy(liste_personnes)))
        if choix == '2':
            print(tri_bulles_inverse(deepcopy(liste_personnes)))
        if choix == '3':
            print(tri_bulles_age(deepcopy(liste_personnes)))
        if choix == 'stop':
            break
choix_fonction()